import WinCalculator from "../game/PokerHand";

import * as controlExamples from './assets/controlExamples';

import {set} from './assets/utils';

controlExamples.examples.forEach(example => {
  let [hand, expectedOutcome, handId] = example;
  let calc = new WinCalculator(set(hand));
  const result = calc.getOutcome();

  test('testing handId = ' + handId, () => {
    expect(result.type).toEqual(expectedOutcome)
  });
});